/**
 * BGE: Basic Growing Environment
 * 
 * This project enables the ability to easily grow
 * plants by computing the plant's environment and
 * notifing the grower when it needs to be watered 
 * or moved in/out of sunlight. 
 * 
 * This program also keeps a record of the data 
 * collected to be used in other machine learning 
 * applications
 * 
 * Hardware Used:
 * 
 * Arduino ATMega 2560 + Bluetooth Shield
 * 2 Soil Humidity Sensors
 * 4 Light sensors
 * Basic growing materials (pots, soil, ect...)
 * 
 * @author Nick Petryna
 * @date April 29th, 2017
 */

const unsigned int HUMIDITY_1_PIN = A0;
const unsigned int HUMIDITY_2_PIN = A1;

/*
    Defines an abstraction over the soil humidity sensor
    @author Nick Petryna
*/

class HumiditySensor {
  
  public:

    HumiditySensor(int pin);
    ~HumiditySensor();

    // Functions
    double get_signal();
    int get_humidity_percentage();
    int get_pin();

    void update();
    void set_signal(double sensor_output);
   
  private:

    // Variables
    int pin;
    int humidity_percentage;
    
    double raw_analog;

    // Functions
    double read_sensor();
};

/*
    Creates a new instance of the humidity sensor
    @author Nick Petryna 
*/

HumiditySensor::HumiditySensor(int pin){
  this->pin = pin;
  this->raw_analog = 0;
}

/*
    Removes an instance of the humidity sensor
    @author Nick Petryna 
*/

HumiditySensor::~HumiditySensor(){
  this->pin = 0;
}

/* 
    Get the pin on which this humidity sensor is operating on
    @author Nick Petryna
*/

int HumiditySensor::get_pin(){
  return this->pin;
}

/* 
    Sets the raw analog signal value of this soil humidity sensor
    @author Nick Petryna
*/

void HumiditySensor::set_signal(double sensor_output){

  // Set the analog data if it appears to be normal
  if(sensor_output > 0){
    this->raw_analog = sensor_output;
  }
}

void HumiditySensor::update(){
  this->set_signal(this->read_sensor());
}

/*
    Gets the soil humidity percentage of this humidity sensor
    @author Nick Petryna 
*/

int HumiditySensor::get_humidity_percentage(){
  return 100;
}

double HumiditySensor::get_signal(){

  if(this->raw_analog > 0){
     return this->raw_analog;
  }

  return 0;
}

double HumiditySensor::read_sensor(){

  if(!this->pin){
    return -1.0;
  }else{
    double analog_out = analogRead(this->pin);
    return analog_out;
  }
}

class Photoresistor {

  public:

    Photoresistor();
    ~Photoresistor();
    
    // Functions
    double get_signal();

    void set_signal(double analog_output);

  private:

    // Variables
    double raw_analog;
  
};

Photoresistor::Photoresistor(){}

Photoresistor::~Photoresistor(){
  this->raw_analog = 0;
}

double Photoresistor::get_signal(){
  return this->raw_analog;
}

void Photoresistor::set_signal(double analog_output){
  //
}

// Declares a new instance for both himidity sensors
HumiditySensor *humidity_sensor_1;
HumiditySensor *humidity_sensor_2;

/*
    Initializes this program
    @author Nick Petryna 
*/

void setup() {

  // Open a serial port
  Serial.begin(9800);

  // Initialize all IO pins
  initialize_pins();

  // Initializes both humidity sensors
  humidity_sensor_1 = new HumiditySensor(HUMIDITY_1_PIN);
  humidity_sensor_2 = new HumiditySensor(HUMIDITY_2_PIN);

  humidity_sensor_1->set_signal(0);
  humidity_sensor_2->set_signal(0);

}

/*
    Initlializes the pin modes for all secondary hardware
    @author Nick Petryna
*/

void initialize_pins(){

  // Initialize all pin modes 
  pinMode(HUMIDITY_1_PIN, INPUT);
  pinMode(HUMIDITY_2_PIN, INPUT);
  
}

void loop() {

  // Update the current state of the sensors
  humidity_sensor_1->update();
  humidity_sensor_2->update();

  Serial.println(humidity_sensor_1->get_humidity_percentage());
  

}
